set(libsoundio_url "https://github.com/andrewrk/libsoundio/archive/7c53dabc6e21d08c68ce6764f3108f6349ced308.zip")
set(libsoundio_local_archive "${CMAKE_CURRENT_SOURCE_DIR}/downloads/libsoundio-7c53dabc6e21d08c68ce6764f3108f6349ced308.zip")
set(libsoundio_local_source "${CMAKE_CURRENT_SOURCE_DIR}/extlib")
set(libsoundio_root "${libsoundio_local_source}/libsoundio-7c53dabc6e21d08c68ce6764f3108f6349ced308")

if (NOT EXISTS "${libsoundio_local_archive}")
  file(DOWNLOAD "${libsoundio_url}" "${libsoundio_local_archive}" SHOW_PROGRESS)
endif()

if (NOT EXISTS "${libsoundio_local_source}")
  execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory "${libsoundio_local_source}")
endif()

if (NOT EXISTS "${libsoundio_root}")
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E tar xzf "${libsoundio_local_archive}"
    WORKING_DIRECTORY "${libsoundio_local_source}"
  )
endif()

option(BUILD_STATIC_LIBS "Build static libraries" ON)
option(BUILD_DYNAMIC_LIBS "Build dynamic libraries" OFF)
option(BUILD_EXAMPLE_PROGRAMS "Build example programs" OFF)
option(BUILD_TESTS "Build tests" OFF)
option(ENABLE_JACK "Enable JACK backend" OFF)
option(ENABLE_PULSEAUDIO "Enable PulseAudio backend" OFF)
option(ENABLE_ALSA "Enable ALSA backend" OFF)
option(ENABLE_COREAUDIO "Enable CoreAudio backend" OFF)
option(ENABLE_WASAPI "Enable WASAPI backend" ON)
add_subdirectory(${libsoundio_root})
