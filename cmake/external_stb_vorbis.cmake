set(stb_vorbis_url "https://raw.githubusercontent.com/nothings/stb/4c519106a7c3862524100f907736c1c74412725d/stb_vorbis.c")
set(stb_vorbis_root "${CMAKE_CURRENT_SOURCE_DIR}/extlib/stb_vorbis-1.09")
set(stb_vorbis_file "${stb_vorbis_root}/stb_vorbis.c")

if (NOT EXISTS "${stb_vorbis_file}")
  file(DOWNLOAD "${stb_vorbis_url}" "${stb_vorbis_file}" SHOW_PROGRESS)
endif()

add_library(stb_vorbis
    "${stb_vorbis_root}/stb_vorbis.c"
)
target_include_directories(stb_vorbis PUBLIC "${stb_vorbis_root}")
