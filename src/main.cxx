#include <iostream>
#include <fstream>
#include "soundio.h"
#include "qmixer.hxx"
#include "qaudiosource.hxx"
#include "qaudiocache.hxx"

int main(int argc, char* argv[]) {
    int sampleRate = 48000;
    qz::QMixer mixer{"QMixerTest", sampleRate};
    qz::QAudioCache audioCache;
    std::shared_ptr<qz::QAudioSource> source1;
    std::shared_ptr<qz::QAudioSource> source2;
    if (argc > 1) {
        source1 = std::make_shared<qz::QAudioSource>(audioCache.load(argv[1]));
    } else {
//        source1 = std::make_shared<qz::QAudioSource>(audioCache.load("jinxed_10.ogg"));
        source2 = std::make_shared<qz::QAudioSource>(audioCache.load("rtrivals.wav"));
    }
    if (source1 != nullptr) mixer.Play(source1);
    if (source2 != nullptr) mixer.Play(source2);
    bool wantToExit = false;
    while (!wantToExit) {
        switch (getc(stdin)) {
        case 'q': wantToExit = true; break;
        case '1': if (source1 != nullptr) source1->flipPause(); break;
        case '2': if (source2 != nullptr) source2->flipPause(); break;
        case 'a': {
            if (source1 != nullptr && !source1->isPaused()) {
                source1->seek(-5 * sampleRate);
            }
            if (source2 != nullptr && !source2->isPaused()) {
                source2->seek(-5 * sampleRate);
            }
            break;
        }
        case 's': {
            if (source1 != nullptr && !source1->isPaused()) {
                source1->seek(5 * sampleRate);
            }
            if (source2 != nullptr && !source2->isPaused()) {
                source2->seek(5 * sampleRate);
            }
            break;
        }
        default: break;
        }
    }
}
