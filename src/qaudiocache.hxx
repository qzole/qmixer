#pragma once

#include <string>
#include <unordered_map>
#include <memory>

namespace qz {

class QAudioClip;

class QAudioCache {
public:
    QAudioCache();
    std::shared_ptr<QAudioClip> load(const std::string& filename);
    std::shared_ptr<QAudioClip> load(const std::string& key, const std::string& filename);
    std::shared_ptr<QAudioClip> get(const std::string& key);
    void remove(const std::string& key);
    void removeUnused();
    void prune();
private:
    std::unordered_map<std::string, std::shared_ptr<QAudioClip>> audioClips;
    std::unordered_map<std::string, std::weak_ptr<QAudioClip>> dyingClips;
};

}
