#pragma once
#include <memory>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <set>
#include "qtrack.hxx"

struct SoundIo;
struct SoundIoDevice;
struct SoundIoOutStream;

namespace qz {

class QAudioSource;
class QAudioClip;

class QMixer {
public:
    QMixer(const std::string& streamName, int sampleRate = 48000);
    ~QMixer();

    void Play(const std::shared_ptr<QAudioClip>& audioClip, float volume = 1.0f);
    void PlayMusic(const std::shared_ptr<QAudioClip>& audioClip, float volume = 1.0f);
private:
    static void writeCallback(struct SoundIoOutStream* outstream, int frameCountMin, int frameCountMax);
    static void underflowCallback(struct SoundIoOutStream* outstream);
    void write(int frameCountMin, int frameCountMax);
    void underflow();

    std::string streamName;
    struct SoundIo* soundio;
    struct SoundIoDevice* device;
    struct SoundIoOutStream* outstream;

    std::set<std::shared_ptr<QAudioSource>> emitters;
};

}
