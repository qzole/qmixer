#include "audiocliprawbuilder.hxx"
#include "audioclipraw.hxx"
#include "qassert.hxx"

namespace qz {

AudioClipRawBuilder::AudioClipRawBuilder(const uint8_t* inputBuffer, size_t inputBufferSize)
    : inputBuffer{inputBuffer}
    , inputBufferSize{inputBufferSize}
    , mSampleRate{0}
    , mChannels{0}
    , mBitDepth{0}
    , mIsSigned{true}
    , mIsLittleEndian{true}
{
    qassert(inputBuffer != nullptr);
    qassert(inputBufferSize > 0);
}

AudioClipRawBuilder& AudioClipRawBuilder::sampleRate(size_t sampleRate) {
    assert(sampleRate > 0);
    mSampleRate = sampleRate;
    return *this;
}

AudioClipRawBuilder& AudioClipRawBuilder::channels(size_t channels) {
    assert(channels > 0);
    mChannels = channels;
    return *this;
}

AudioClipRawBuilder& AudioClipRawBuilder::bitDepth(size_t bitDepth) {
    assert(bitDepth > 0);
    assert(bitDepth <= 32);
    assert((bitDepth % 8) == 0);
    mBitDepth = bitDepth;
    return *this;
}

AudioClipRawBuilder& AudioClipRawBuilder::isSigned(bool isSigned) {
    mIsSigned = isSigned;
    return *this;
}

AudioClipRawBuilder& AudioClipRawBuilder::isLittleEndian(bool isLittleEndian) {
    mIsLittleEndian = isLittleEndian;
    return *this;
}

const static uint32_t signExtendMasks[] = {
    0x00000000,
    0xff000000,
    0xffff0000,
    0xffffff00,
};

template<uint8_t bitDepth, bool isSigned, bool isLittleEndian>
static void convertIntoTemplate(const uint8_t* rawBuffer, float* targetBuffer, size_t targetBufferSize, size_t channels) {
    uint8_t byteDepth{bitDepth / 8};
    float divider{((int32_t)1) << (bitDepth - 1)};
    for (size_t ch = 0; ch < channels; ++ch) {
        size_t tStart = ch * targetBufferSize / channels;
        for (size_t i = 0; i < targetBufferSize / channels; ++i) {
            int32_t intValue = 0;
            const uint8_t* rawValue = &rawBuffer[i * byteDepth * channels + ch];
            if(isSigned && (rawValue[byteDepth - 1] & 0x80)) { // sign extend
                intValue |= signExtendMasks[4 - byteDepth];
            }
            if (isLittleEndian) {
                for (int j = 0; j < byteDepth; ++j) {
                    intValue |= rawValue[j] << (j * 8);
                }
            } else {
                for (int j = 0; j < byteDepth; ++j) {
                    intValue |= rawValue[j] << ((byteDepth - j - 1) * 8);
                }
            }
            if (!isSigned) {
                intValue -= divider;
            }
            targetBuffer[tStart + i] = (float)intValue / divider;
        }
    }
}

template<size_t bitDepth>
void AudioClipRawBuilder::convertIntoWithBitDepth(float* targetBuffer, size_t targetBufferSize)
{
    if (mIsSigned) {
        if (mIsLittleEndian) {
            convertIntoTemplate<bitDepth, true, true>(inputBuffer, targetBuffer, targetBufferSize, mChannels);
        } else {
            convertIntoTemplate<bitDepth, true, false>(inputBuffer, targetBuffer, targetBufferSize, mChannels);
        }
    } else {
        if (mIsLittleEndian) {
            convertIntoTemplate<bitDepth, false, true>(inputBuffer, targetBuffer, targetBufferSize, mChannels);
        } else {
            convertIntoTemplate<bitDepth, false, false>(inputBuffer, targetBuffer, targetBufferSize, mChannels);
        }
    }
}

std::unique_ptr<AudioClipRaw> AudioClipRawBuilder::build() {
    qassert(inputBuffer != nullptr);
    assert(mSampleRate > 0);
    assert(mChannels > 0);
    assert(mBitDepth > 0);

    size_t resultBufferSize = inputBufferSize / (mBitDepth / 8);
    std::unique_ptr<float[]> resultBuffer{new float[resultBufferSize]};
    switch (mBitDepth) {
    case 8:
        convertIntoWithBitDepth<8>(resultBuffer.get(), resultBufferSize);
        break;
    case 16:
        convertIntoWithBitDepth<16>(resultBuffer.get(), resultBufferSize);
        break;
    case 24:
        convertIntoWithBitDepth<24>(resultBuffer.get(), resultBufferSize);
        break;
    case 32:
        convertIntoWithBitDepth<32>(resultBuffer.get(), resultBufferSize);
        break;
    default:
        qassert(false);
        break;
    }
    return std::make_unique<AudioClipRaw>(std::move(resultBuffer), resultBufferSize, mSampleRate, mBitDepth, mChannels);
}


}
