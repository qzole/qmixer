#include "qaudioclip.hxx"
#include <fstream>
#include <sstream>
#include <string>
#include <limits>
#include <vector>
#include <string.h>
#include <algorithm>
#include "qassert.hxx"
#include "stb_vorbis.c"

namespace qz {

QAudioClip::QAudioClip()
//    : sampleRate{0}
//    , originalBitDepth{0}
//    , channels{0}
//    , vorbisHandle{nullptr}
//    , vorbisSamplesCount{0}
//    , lastCursor{0}
{

}

//static const size_t remainderSize = 1000;
//struct Wav {
////    char riffStr[4]; already read when file type is determined
//    uint32_t fileSize;
//    char waveStr[4];
//    char fmtStr[4];
//    uint32_t fmtSize;
//    uint16_t fmt;
//    uint16_t channels;
//    uint32_t sampleRate;
//    uint32_t bytesPerSec;
//    uint16_t blockAlign;
//    uint16_t bitsPerSample;
//    uint8_t remainder[remainderSize];
//};

//QAudioClip::QAudioClip(const char* pathToAudioFile) {
//    std::ifstream ifStream{pathToAudioFile, std::ios::binary|std::ios::ate};
//    if (!ifStream.is_open()) {
//        throw runtimeErrorWithMsg("Couldn't open file: ", pathToAudioFile);
//    } else {
//        std::streamsize fileSize = ifStream.tellg();
//        if (fileSize < std::streamoff{4}) {
//            throw std::runtime_error("Invalid vorbis file, error on file operation or too small");
//        }
//        if (fileSize > std::numeric_limits<int>::max()) {
//            throw std::runtime_error("Invalid vorbis file, too large");
//        }
//        ifStream.seekg(0, std::ios::beg);
//        char capturePattern[4];
//        ifStream.read(capturePattern, 4);
//        if (strcmp(capturePattern, "OggS") == 0) {
//            ifStream.seekg(0, std::ios::beg);
//            bufferSize = fileSize;
//            fileBuffer.reset(new unsigned char[bufferSize]);
//            ifStream.read((char*)fileBuffer.get(), fileSize);
//            ifStream.close();
//            int error = VORBIS__no_error;
//            vorbisHandle = stb_vorbis_open_memory((const unsigned char*)fileBuffer.get(), fileSize, &error, nullptr);
//            if (vorbisHandle == nullptr) {
//                throw runtimeErrorWithMsg("Couldn't decode file as vorbis, error code: ", error);
//            }
//            stb_vorbis_info info = stb_vorbis_get_info(vorbisHandle);
//            sampleRate = info.sample_rate;
//            channels = info.channels;
//            vorbisSamplesCount = stb_vorbis_stream_length_in_samples(vorbisHandle);
//        } else if (strcmp(capturePattern, "RIFF") == 0) {
//            if ((size_t)fileSize < sizeof(Wav)) {
//                throw std::runtime_error("Invalid wav file, too small");
//            }
//            Wav wav;
//            ifStream.read((char*)&wav, sizeof(Wav));
//            static const uint8_t dataStr[4] = {'d', 'a', 't', 'a'};
//            const uint8_t* foundDataStr = std::search(wav.remainder, wav.remainder + remainderSize, dataStr, dataStr + 4);
//            if (foundDataStr == nullptr) {
//                throw std::runtime_error("Invalid wav file, could not find \"data\"");
//            }
//            uint32_t dataSize = *((uint32_t*)(foundDataStr + 4)); // TODO this will fail on big endian systems...
//            std::unique_ptr<uint8_t[]> memblock{new uint8_t[dataSize]};
//            auto startOfData = sizeof(Wav) - remainderSize + (foundDataStr - wav.remainder) + 8;
//            ifStream.seekg(startOfData, std::ios::beg);
//            ifStream.read((char*)memblock.get(), dataSize);
//            ifStream.close();

//            RawConverter converter;
//            converter.bitDepth(wav.bitsPerSample)
//                    .buffer(memblock.get(), dataSize)
//                    .channels(wav.channels)
//                    .sampleRate(wav.sampleRate);
//            // TODO set signdness and endianness
//            buffer.reset(converter.convertInto(bufferSize));
//        } else {
//            throw runtimeErrorWithMsg("Invalid audio file: ", pathToAudioFile);
//        }
//    }
//}

//QAudioClip::~QAudioClip() {
//    stb_vorbis_close(vorbisHandle);
//}

//size_t QAudioClip::addSamples(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRate) {
//    if (vorbisHandle != nullptr) {
//        return addSamplesVorbis(output, cursor, sampleCount, channelCount, sampleRate);
//    } else {
//        return addSamplesWav(output, cursor, sampleCount, channelCount, sampleRate);
//    }
//}

//size_t QAudioClip::addSamplesVorbis(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRateRequested) {
//    qassert(channelCount >= 1);
//    qassert(sampleRateRequested > 10 && sampleRateRequested < 500000);
//    int channelsToFill = std::min(channelCount, (int)channels);
//    if ((size_t)sampleRateRequested == sampleRate) {
//        if (lastCursor != cursor) {
//            if (!stb_vorbis_seek(vorbisHandle, cursor)) {
//                lastCursor = std::numeric_limits<size_t>::max();
//                return 0;
//            }
//        }
//        std::vector<float*> buffers(channelsToFill);
//        for (int channel = 0; channel < channelsToFill; ++channel) {
//            buffers[channel] = output + channel * sampleCount;
//        }
//        int ret = stb_vorbis_get_samples_float(vorbisHandle, channelsToFill, buffers.data(), sampleCount);
//        assert(ret >= 0);
//        lastCursor = cursor + ret;
//        return (size_t)ret;
//    } else {
//        float sampleRateRatio = (float)sampleRateRequested / (float)sampleRate;
//        size_t ownCursor = cursor / sampleRateRatio;
//        size_t samplesRemain = vorbisSamplesCount - ownCursor;
//        size_t ownSamplesAdded = std::min(samplesRemain, (size_t)(sampleCount / sampleRateRatio));
//        size_t samplesAdded = ownSamplesAdded * sampleRateRatio;

//        if (lastCursor != ownCursor) {
//            if (!stb_vorbis_seek(vorbisHandle, ownCursor)) {
//                lastCursor = std::numeric_limits<size_t>::max();
//                return 0;
//            }
//        }
//        float** buffers = new float*[channelsToFill];
//        for (int channel = 0; channel < channelsToFill; ++channel) {
//            buffers[channel] = new float[ownSamplesAdded];
//        }
//#ifndef NDEBUG
//        int ret =
//#endif
//        stb_vorbis_get_samples_float(vorbisHandle, channelsToFill, buffers, ownSamplesAdded);
//        assert(ret >= 0);
//        assert((size_t)ret == ownSamplesAdded);

//        for (int channel = 0; channel < channelsToFill; channel += 1) {
//            float* out = output + channel * sampleCount;
//            for (size_t i = 0; i < samplesAdded; ++i) {
//                float fOffset = i / sampleRateRatio;
//                size_t offset = (size_t)fOffset;
//                float betweenSamplesRatio = fOffset - offset;
//                size_t ownLowIndex = ((size_t)offset);
//                size_t ownHighIndex = std::min(ownLowIndex + 1, ownSamplesAdded - 1); // todo maybe std::min is not neccesary.
//                float sampleLeft = buffers[channel][ownLowIndex];
//                float sampleRight = buffers[channel][ownHighIndex];
//                float sample = lerp(sampleLeft, sampleRight, betweenSamplesRatio);
//                out[i] += sample;
//            }
//        }
//        if (ownSamplesAdded == samplesRemain) {
//            lastCursor = 0;
//        } else {
//            lastCursor = ownCursor + ownSamplesAdded;
//        }
//        for (int channel = 0; channel < channelsToFill; ++channel) {
//            delete[] buffers[channel];
//        }
//        delete[] buffers;
//        return samplesAdded;
//    }
//}

//size_t QAudioClip::addSamplesWav(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRateRequested) {
//    qassert(channelCount >= 1);
//    qassert(sampleRateRequested > 10 && sampleRateRequested < 500000);
//    size_t bufferSampleSize = bufferSize / channels;
//    int channelsToFill = std::min(channelCount, (int)channels);
//    if ((size_t)sampleRateRequested == sampleRate) {
//        qassert(cursor <= bufferSampleSize);
//        size_t samplesRemain = bufferSampleSize - cursor;
//        size_t samplesAdded = std::min(samplesRemain, sampleCount);
//        for (int channel = 0; channel < channelsToFill; channel += 1) {
//            float* out = output + channel * sampleCount;
//            for (size_t i = 0; i < samplesAdded; ++i) {
//                out[i] += buffer[(cursor + i) * channels + channel];
//            }
//        }
//        return samplesAdded;
//    } else {
//        float sampleRateRatio = (float)sampleRateRequested / (float)sampleRate;
//        size_t resampledBufferSampleSize = bufferSize * sampleRateRatio / channels;
//        size_t resampledSamplesRemain = resampledBufferSampleSize - cursor;
//        size_t samplesAdded = std::min((size_t)resampledSamplesRemain, sampleCount);
//        size_t ownCursor = cursor / sampleRateRatio;
//        for (int channel = 0; channel < channelsToFill; channel += 1) {
//            float* out = output + channel * sampleCount;
//            for (size_t i = 0; i < samplesAdded; ++i) {
//                float fOffset = i / sampleRateRatio;
//                size_t offset = (size_t)fOffset;
//                float betweenSamplesRatio = fOffset - offset;
//                size_t ownLowIndex = (ownCursor + (size_t)offset);
//                size_t ownHighIndex = std::min(ownLowIndex + 1, bufferSampleSize - 1); // todo maybe std::min is not neccesary.
//                float sampleLeft = buffer[ownLowIndex * channels + channel];
//                float sampleRight = buffer[ownHighIndex * channels + channel];
//                float sample = lerp(sampleLeft, sampleRight, betweenSamplesRatio);
//                out[i] += sample;
//            }
//        }
//        return samplesAdded;
//    }
//}

//RawConverter::RawConverter()
//    : inputBuffer{nullptr}
//    , inputBufferSize{0}
//    , mSampleRate{0}
//    , mChannels{0}
//    , mBitDepth{0}
//    , mIsSigned{true}
//    , mIsLittleEndian{true}
//{

//}

//RawConverter& RawConverter::buffer(const uint8_t* inputBuffer, size_t inputBufferSize) {
//    qassert(inputBuffer != nullptr);
//    qassert(inputBufferSize > 0);
//    this->inputBuffer = inputBuffer;
//    this->inputBufferSize = inputBufferSize;
//    return *this;
//}

//RawConverter& RawConverter::sampleRate(size_t sampleRate) {
//    assert(sampleRate > 0);
//    mSampleRate = sampleRate;
//    return *this;
//}

//RawConverter& RawConverter::channels(size_t channels) {
//    assert(channels > 0);
//    mChannels = channels;
//    return *this;
//}

//RawConverter& RawConverter::bitDepth(size_t bitDepth) {
//    assert(bitDepth > 0);
//    assert(bitDepth <= 32);
//    assert((bitDepth % 8) == 0);
//    mBitDepth = bitDepth;
//    return *this;
//}

//RawConverter& RawConverter::isSigned(bool isSigned) {
//    mIsSigned = isSigned;
//    return *this;
//}

//RawConverter& RawConverter::isLittleEndian(bool isLittleEndian) {
//    mIsLittleEndian = isLittleEndian;
//    return *this;
//}

//const static uint32_t signExtendMasks[] = {
//    0x00000000,
//    0xff000000,
//    0xffff0000,
//    0xffffff00,
//};

//template<uint8_t bitDepth, bool isSigned, bool isLittleEndian>
//static void convertIntoTemplate(const uint8_t* rawBuffer, float* targetBuffer, size_t targetBufferSize) {
//    uint8_t byteDepth{bitDepth / 8};
//    float divider{((int32_t)1) << (bitDepth - 1)};
//    for (size_t i = 0; i < targetBufferSize; ++i) {
//        int32_t intValue = 0;
//        const uint8_t* rawValue = &rawBuffer[i * byteDepth];
//        if(isSigned && (rawValue[byteDepth - 1] & 0x80)) { // sign extend
//            intValue |= signExtendMasks[4 - byteDepth];
//        }
//        if (isLittleEndian) {
//            for (int j = 0; j < byteDepth; ++j) {
//                intValue |= rawValue[j] << (j * 8);
//            }
//        } else {
//            for (int j = 0; j < byteDepth; ++j) {
//                intValue |= rawValue[j] << ((byteDepth - j - 1) * 8);
//            }
//        }
//        if (!isSigned) {
//            intValue -= divider;
//        }
//        targetBuffer[i] = (float)intValue / divider;
//    }
//}

//template<size_t bitDepth>
//void RawConverter::convertIntoWithBitDepth(float* targetBuffer, size_t targetBufferSize)
//{
//    if (mIsSigned) {
//        if (mIsLittleEndian) {
//            convertIntoTemplate<bitDepth, true, true>(inputBuffer, targetBuffer, targetBufferSize);
//        } else {
//            convertIntoTemplate<bitDepth, true, false>(inputBuffer, targetBuffer, targetBufferSize);
//        }
//    } else {
//        if (mIsLittleEndian) {
//            convertIntoTemplate<bitDepth, false, true>(inputBuffer, targetBuffer, targetBufferSize);
//        } else {
//            convertIntoTemplate<bitDepth, false, false>(inputBuffer, targetBuffer, targetBufferSize);
//        }
//    }
//}

//float* RawConverter::convertInto(size_t& resultBufferSize) {
//    qassert(inputBuffer != nullptr);
//    assert(mSampleRate > 0);
//    assert(mChannels > 0);
//    assert(mBitDepth > 0);

//    resultBufferSize = inputBufferSize / (mBitDepth / 8);
//    float* resultBuffer = new float[resultBufferSize];
//    switch (mBitDepth) {
//    case 8:
//        convertIntoWithBitDepth<8>(resultBuffer, resultBufferSize);
//        break;
//    case 16:
//        convertIntoWithBitDepth<16>(resultBuffer, resultBufferSize);
//        break;
//    case 24:
//        convertIntoWithBitDepth<24>(resultBuffer, resultBufferSize);
//        break;
//    case 32:
//        convertIntoWithBitDepth<32>(resultBuffer, resultBufferSize);
//        break;
//    default:
//        qassert(false);
//        break;
//    }
//    return resultBuffer;
//}

}
