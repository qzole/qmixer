#pragma once

#include <cassert>
#include <sstream>

#define qassert assert

namespace qz {

inline void concatErrorMsg(std::stringstream&) {}

template<typename T, typename ... Types>
void concatErrorMsg(std::stringstream& ss, T firstArg, Types ... args) {
    ss << firstArg;
    concatErrorMsg(ss, args...);
}

template<typename ... Types>
std::runtime_error runtimeErrorWithMsg(Types ... args) {
    std::stringstream ss;
    concatErrorMsg(ss, args...);
    return std::runtime_error(ss.str());
}

template<typename LerpedType, typename OtherType>
LerpedType lerp(LerpedType a, LerpedType b, OtherType f) {
    return (a * (1.0f - f)) + (b * f);
}

}
