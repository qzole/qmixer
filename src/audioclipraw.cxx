#include "audioclipraw.hxx"
#include <algorithm>
#include "qassert.hxx"

namespace qz {

AudioClipRaw::AudioClipRaw(std::unique_ptr<float[]> buffer, size_t bufferSize, size_t sampleRate, size_t originalBitDepth, size_t channels)
    : buffer{std::move(buffer)}
    , bufferSize{bufferSize}
    , sampleCount{bufferSize / channels}
    , sampleRate{sampleRate}
    , originalBitDepth{originalBitDepth}
    , channels{channels}
{
    qassert(this->buffer != nullptr);
    qassert(bufferSize > 0);
}

size_t AudioClipRaw::addSamples(float* output, size_t cursor, size_t sampleCountRequested, int channelCount, int sampleRateRequested, float volume) {
    qassert(channelCount >= 1);
    qassert(sampleRateRequested > 10 && sampleRateRequested < 500000);
    int channelsToFill = std::min(channelCount, (int)channels);
    if ((size_t)sampleRateRequested == sampleRate) {
        qassert(cursor <= sampleCount);
        size_t samplesRemain = sampleCount - cursor;
        size_t samplesAdded = std::min(samplesRemain, sampleCountRequested);
        for (int channel = 0; channel < channelsToFill; channel += 1) {
            float* out = output + channel * sampleCountRequested;
            float* in = buffer.get() + channel * sampleCount + cursor;
            for (size_t i = 0; i < samplesAdded; ++i) {
                out[i] += in[i] * volume;
            }
        }
        return samplesAdded;
    } else {
        float sampleRateRatio = (float)sampleRateRequested / (float)sampleRate;
        size_t resampledBufferSampleSize = bufferSize * sampleRateRatio / channels;
        size_t resampledSamplesRemain = resampledBufferSampleSize - cursor;
        size_t samplesAdded = std::min((size_t)resampledSamplesRemain, sampleCountRequested);
        for (int channel = 0; channel < channelsToFill; channel += 1) {
            float* out = output + channel * sampleCountRequested;
            float* in = buffer.get() + channel * sampleCount;
            for (size_t i = 0; i < samplesAdded; ++i) {
                float fOffset = (cursor + i) / sampleRateRatio;
                size_t offset = (size_t)fOffset;
                float betweenSamplesRatio = fOffset - offset;
                float otherValue = offset + 1 < sampleCount - 1 ? in[offset + 1] : 0;
                out[i] += lerp(in[offset], otherValue, betweenSamplesRatio) * volume;
            }
        }
        return samplesAdded;
    }
}

}
