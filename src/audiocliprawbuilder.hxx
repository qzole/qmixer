#pragma once

#include <memory>

namespace qz {

class AudioClipRaw;

class AudioClipRawBuilder {
public:
    AudioClipRawBuilder(const uint8_t* inputBuffer, size_t inputBufferSize);
    AudioClipRawBuilder& sampleRate(size_t sampleRate);
    AudioClipRawBuilder& channels(size_t channels);
    AudioClipRawBuilder& bitDepth(size_t bitDepth);
    AudioClipRawBuilder& isSigned(bool isSigned);
    AudioClipRawBuilder& isLittleEndian(bool isLittleEndian);
    std::unique_ptr<AudioClipRaw> build();
private:
    template<size_t bitDepth> void convertIntoWithBitDepth(float* targetBuffer, size_t targetBufferSize);

    const uint8_t* inputBuffer;
    const size_t inputBufferSize;
    size_t mSampleRate;
    size_t mChannels;
    size_t mBitDepth;
    bool mIsSigned;
    bool mIsLittleEndian;
};

}
