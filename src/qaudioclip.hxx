#pragma once

#include <cstdint>

namespace qz {

class QAudioClip {
public:
    QAudioClip();
    QAudioClip(const QAudioClip&) = delete;
    QAudioClip& operator=(QAudioClip&) = delete;
    QAudioClip(QAudioClip&&) = default;
    QAudioClip& operator=(QAudioClip&&) = default;
    virtual ~QAudioClip() = default;

    virtual size_t addSamples(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRate, float volume) = 0;
};

}
