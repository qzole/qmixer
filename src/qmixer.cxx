#include "qmixer.hxx"
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include "soundio.h"
#include "qaudiosource.hxx"
#include "qassert.hxx"

namespace qz {

QMixer::QMixer(const std::string& streamName, int sampleRate)
    : streamName{streamName}
    , soundio{nullptr}
    , device{nullptr}
    , outstream{nullptr}
{
    soundio = soundio_create();
    if (!soundio) {
        throw std::bad_alloc();
    }
    soundio->userdata = this;

    int err = soundio_connect(soundio);
    if (err) {
        soundio_destroy(soundio);
        std::stringstream ss;
        ss << "Unable to connect to backend: " << soundio_strerror(err);
        throw std::runtime_error(ss.str());
    }

    soundio_flush_events(soundio);

    int selected_device_index = soundio_default_output_device_index(soundio);
    if (selected_device_index < 0) {
        soundio_destroy(soundio);
        throw std::runtime_error("Output device not found");
    }
    device = soundio_get_output_device(soundio, selected_device_index);
    if (!device) {
        throw std::bad_alloc();
    }
    if (device->probe_error) {
        std::stringstream ss;
        ss << "Cannot probe device: " << soundio_strerror(device->probe_error);
        soundio_device_unref(device);
        soundio_destroy(soundio);
        throw std::runtime_error(ss.str());
    }
    outstream = soundio_outstream_create(device);
    if (!outstream) {
        throw std::bad_alloc();
    }

    outstream->name = streamName.c_str();
    outstream->write_callback = writeCallback;
    outstream->underflow_callback = underflowCallback;
    outstream->software_latency = 1.0f / 30.0f;
    outstream->sample_rate = sampleRate;

    if (soundio_device_supports_format(device, SoundIoFormatFloat32NE)) {
        outstream->format = SoundIoFormatFloat32NE;
    } else {
        soundio_outstream_destroy(outstream);
        soundio_device_unref(device);
        soundio_destroy(soundio);
        throw std::runtime_error("No suitable device format available");
    }

    if ((err = soundio_outstream_open(outstream))) {
        std::stringstream ss;
        ss << "Unable to open device: " << soundio_strerror(err);
        soundio_outstream_destroy(outstream);
        soundio_device_unref(device);
        soundio_destroy(soundio);
        throw std::runtime_error(ss.str());
    }

    if (outstream->layout_error) {
        std::stringstream ss;
        ss << "Unable to set channel layout: " << soundio_strerror(outstream->layout_error);
        soundio_outstream_destroy(outstream);
        soundio_device_unref(device);
        soundio_destroy(soundio);
        throw std::runtime_error(ss.str());
    }

    if ((err = soundio_outstream_start(outstream))) {
        std::stringstream ss;
        ss << "Unable to start device: " << soundio_strerror(err);
        soundio_outstream_destroy(outstream);
        soundio_device_unref(device);
        soundio_destroy(soundio);
        throw std::runtime_error(ss.str());
    }
}

QMixer::~QMixer() {
    soundio_outstream_destroy(outstream);
    soundio_device_unref(device);
    soundio_destroy(soundio);
}

void QMixer::Play(const std::shared_ptr<QAudioClip>& audioClip, float volume) {
    emitters.emplace(std::make_shared<QAudioSource>(audioClip, volume, false));
}

void QMixer::PlayMusic(const std::shared_ptr<QAudioClip>& audioClip, float volume) {
    emitters.emplace(std::make_shared<QAudioSource>(audioClip, volume, true));
}

void QMixer::writeCallback(struct SoundIoOutStream* outstream, int frameCountMin, int frameCountMax) {
    QMixer* qmixer = (QMixer*)outstream->device->soundio->userdata;
    qmixer->write(frameCountMin, frameCountMax);
}

void QMixer::underflowCallback(struct SoundIoOutStream* outstream) {
    QMixer* qmixer = (QMixer*)outstream->device->soundio->userdata;
    qmixer->underflow();
}

template<class T>
constexpr const T& clamp(const T& v, const T& lo, const T& hi) {
    return v < lo ? lo : hi < v ? hi : v;
}

void QMixer::write(int frameCountMin, int frameCountMax) {
    struct SoundIoChannelArea *areas;
    int err;
    int framesLeft = clamp(outstream->sample_rate / 30, frameCountMin, frameCountMax);

	for (;;) {
		int frameCount = framesLeft;
		if ((err = soundio_outstream_begin_write(outstream, &areas, &frameCount))) {
			fprintf(stderr, "unrecoverable stream error: %s\n", soundio_strerror(err));
			exit(1);
		}

		if (!frameCount) {
			break;
		}

		const struct SoundIoChannelLayout* layout = &outstream->layout;
		std::vector<float> samples(frameCount * layout->channel_count);
		for (auto it = emitters.begin(); it != emitters.end();) {
			const std::shared_ptr<QAudioSource>& emitter = *it;
			emitter->addNextSamples(samples.data(), frameCount, layout->channel_count, outstream->sample_rate);
			if (emitter->hasEnded()) {
				it = emitters.erase(it);
			}
			else {
				++it;
			}
		}
		for (int channel = 0; channel < layout->channel_count; channel += 1) {
			SoundIoChannelArea& area = areas[channel];
			for (int frame = 0; frame < frameCount; frame += 1) {
				(*((float*)area.ptr)) = samples[frame]; // TODO optimize if area.step == sizeof(float)
				area.ptr += area.step;
			}
		}

		if ((err = soundio_outstream_end_write(outstream))) {
			if (err == SoundIoErrorUnderflow) return;
			fprintf(stderr, "unrecoverable stream error: %s\n", soundio_strerror(err));
			exit(1);
		}

		framesLeft -= frameCount;
		if (framesLeft <= 0) {
			break;
		}
    }
}

void QMixer::underflow() {
    // TODO
}

}
