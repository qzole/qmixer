#pragma once

#include <memory>
#include <vector>

class QAudioSource;

class QTrack {
public:
    QTrack();
    //void addSource(const std::shared_ptr<QAudioSource> audioSource);
    //void removeSource(const std::shared_ptr<QAudioSource> audioSource);
private:
    std::vector<std::shared_ptr<QAudioSource>> audioSources;
};
