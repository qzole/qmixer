#pragma once

#include <memory>

namespace qz {

class QAudioClip;

class AudioClipLoader {
public:
    static std::unique_ptr<QAudioClip> load(const char* pathToAudioFile);
};

}
