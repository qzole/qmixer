#pragma once

#include <memory>

namespace qz {

class QAudioClip;

class QAudioSource {
public:
    QAudioSource(const std::shared_ptr<QAudioClip>& audioClip, float volume = 1.0f, bool isLooping = false);
    QAudioSource(const QAudioSource& other) = delete;
    QAudioSource& operator=(const QAudioSource& other) = delete;
    QAudioSource(QAudioSource&& other) = delete;
    QAudioSource& operator=(QAudioSource&& other) = delete;
    bool hasEnded() const;
    void addNextSamples(float* output, size_t size, int channels, int sampleRate);
    void pause(bool paused) { this->paused = paused; }
    bool isPaused() { return this->paused; }
    void flipPause() { paused = !paused; }
    void seek(long long int amount);
private:
    float volume;
    bool isLooping;

    size_t cursor;
    bool paused;
    std::shared_ptr<QAudioClip> audioClip;
};

}
