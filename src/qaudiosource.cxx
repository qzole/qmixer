#include "qaudiosource.hxx"
#include "qaudioclip.hxx"
#include "qassert.hxx"

namespace qz {

QAudioSource::QAudioSource(const std::shared_ptr<QAudioClip>& audioClip, float volume, bool isLooping)
    : volume{volume}
    , isLooping(isLooping)
    , cursor{0}
    , paused{false}
    , audioClip{audioClip}
{
    qassert(audioClip != nullptr);
}

bool QAudioSource::hasEnded() const {
    return !isLooping && paused;
}

void QAudioSource::addNextSamples(float* output, size_t size, int channels, int sampleRate) {
    if (paused) return;
#ifndef NDEBUG
    bool previousWasZero = false;
#endif
    while (size > 0) {
        size_t samplesAdded = audioClip->addSamples(output, cursor, size, channels, sampleRate, volume);
#ifndef NDEBUG
        qassert((!previousWasZero || samplesAdded > 0) && samplesAdded <= size);
        previousWasZero = samplesAdded == 0;
#endif
        if (samplesAdded < size) {
            cursor = 0;
            if (!isLooping)
            {
                paused = true;
                return;
            }
        } else {
            cursor += samplesAdded;
        }
        output += samplesAdded;
        size -= samplesAdded;
    }
}

void QAudioSource::seek(long long amount) {
    if (amount < 0) {
        if ((size_t)-amount > cursor) {
            cursor = 0;
        } else {
            cursor += amount;
        }
    } else {
        cursor += amount;
    }
}

}
