#pragma once

#include "qaudioclip.hxx"
#include <memory>

namespace qz {

class AudioClipRaw : public QAudioClip {
public:
    AudioClipRaw(std::unique_ptr<float[]> buffer, size_t bufferSize, size_t sampleRate, size_t originalBitDepth, size_t channels);
    size_t addSamples(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRateRequested, float volume) override;
private:
    std::unique_ptr<float[]> buffer;
    size_t bufferSize;
    size_t sampleCount;
    size_t sampleRate;
    size_t originalBitDepth;
    size_t channels;
};

}
