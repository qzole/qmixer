#include "audioclipvorbis.hxx"
#include <limits>
#include <vector>
#include <algorithm>
#include "stb_vorbis.c"
#include "qassert.hxx"

namespace qz {

AudioClipVorbis::AudioClipVorbis(std::unique_ptr<uint8_t[]> fileBufferIn, size_t fileSize)
    : fileBuffer{std::move(fileBufferIn)}
    , sampleRate{0}
    , channels{0}
    , sampleCount{0}
    , lastCursor{0}
{
    qassert(fileBuffer != nullptr);
    int error = VORBIS__no_error;
    vorbisHandle = stb_vorbis_open_memory((const unsigned char*)fileBuffer.get(), fileSize, &error, nullptr);
    if (vorbisHandle == nullptr) {
        throw runtimeErrorWithMsg("Couldn't decode file as vorbis, error code: ", error);
    }
    stb_vorbis_info info = stb_vorbis_get_info(vorbisHandle);
    sampleRate = info.sample_rate;
    channels = info.channels;
    sampleCount = stb_vorbis_stream_length_in_samples(vorbisHandle);
}

size_t AudioClipVorbis::addSamples(float* output, size_t cursor, size_t sampleCountRequested, int channelCount, int sampleRateRequested, float volume) {
    qassert(channelCount >= 1);
    qassert(sampleRateRequested > 10 && sampleRateRequested < 500000);
    int channelsToFill = std::min(channelCount, (int)channels);
    if ((size_t)sampleRateRequested == sampleRate) {
        if (lastCursor != cursor) {
            if (!stb_vorbis_seek(vorbisHandle, cursor)) {
                lastCursor = std::numeric_limits<size_t>::max();
                return 0;
            }
        }

        std::vector<float*> buffers(channelsToFill);
        for (int channel = 0; channel < channelsToFill; ++channel) {
            buffers[channel] = internalBuffer + channel * sampleCountRequested;
            float* buffer = buffers[channel];
            for (size_t i = 0; i < sampleCountRequested; ++i) {
                buffer[i] = 0.0f;
            }
        }

        int ret = stb_vorbis_get_samples_float(vorbisHandle, channelsToFill, buffers.data(), sampleCountRequested);
        assert(ret >= 0);
        for (int channel = 0; channel < channelsToFill; ++channel) {
            float* out = output + channel * sampleCountRequested;
            for (int i = 0; i < ret; ++i) {
                out[i] += buffers[channel][i] * volume;
            }
        }

        lastCursor = cursor + ret;
        return (size_t)ret;
    } else {
        float sampleRateRatio = (float)sampleRateRequested / (float)sampleRate;
        size_t ownCursor = cursor / sampleRateRatio;
        size_t samplesRemain = sampleCount - ownCursor;
        size_t ownSamplesAdded = std::min(samplesRemain, (size_t)(sampleCountRequested / sampleRateRatio) + 1); // TODO this leaks 1 sample, sub-samples needs to be cached to avoid seeking...
        size_t samplesAdded = std::min(sampleCountRequested, (size_t)(ownSamplesAdded * sampleRateRatio));

        if (lastCursor != ownCursor) {
            if (!stb_vorbis_seek(vorbisHandle, ownCursor)) {
                lastCursor = std::numeric_limits<size_t>::max();
                return 0;
            }
        }
        std::vector<float*> buffers(channelsToFill);
        for (int channel = 0; channel < channelsToFill; ++channel) {
            buffers[channel] = internalBuffer + channel * ownSamplesAdded;
            float* buffer = buffers[channel];
            for(size_t i = 0; i < ownSamplesAdded; ++i) {
                buffer[i] = 0.0f;
            }
        }
        int ret = stb_vorbis_get_samples_float(vorbisHandle, channelsToFill, buffers.data(), ownSamplesAdded);
        assert(ret >= 0);
        assert((size_t)ret == ownSamplesAdded);

        for (int channel = 0; channel < channelsToFill; channel += 1) {
            float* buffer = buffers[channel];
            float* out = output + channel * sampleCountRequested;
            for (size_t i = 0; i < samplesAdded; ++i) {
                float fOffset = ((cursor + i) / sampleRateRatio) - ownCursor;
                size_t offset = (size_t)fOffset;
                float betweenSamplesRatio = fOffset - offset;
                float sampleRight = offset + 1 < ownSamplesAdded - 1 ? buffer[offset + 1] : 0;
                float sample = lerp(buffer[offset], sampleRight, betweenSamplesRatio) * volume;
                out[i] += sample;
            }
        }
        lastCursor = ownCursor + ret;
        return samplesAdded;
    }
}

}
