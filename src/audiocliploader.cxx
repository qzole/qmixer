#include "audiocliploader.hxx"
#include <fstream>
#include <limits>
#include <string.h>
#include <algorithm>
#include <stdint.h>
#include "qassert.hxx"
#include "audioclipraw.hxx"
#include "audiocliprawbuilder.hxx"
#include "audioclipvorbis.hxx"
#include "endian.h"

namespace qz {

static uint16_t parseUint16LE(const uint8_t* source) {
#if defined(SOUNDIO_OS_BIG_ENDIAN)
    uint16_t result = 0;
    result |= (*source + 0) << 8;
    result |= (*source + 1);
    return result;
#elif defined (SOUNDIO_OS_LITTLE_ENDIAN)
    return *((uint16_t*)source);
#else
#error unknown byte order
#endif
}

static uint32_t parseUint32LE(const uint8_t* source) {
#if defined(SOUNDIO_OS_BIG_ENDIAN)
    uint16_t result = 0;
    result |= (*source + 0) << 24;
    result |= (*source + 1) << 16;
    result |= (*source + 2) <<  8;
    result |= (*source + 3);
    return result;
#elif defined (SOUNDIO_OS_LITTLE_ENDIAN)
    return *((uint32_t*)source);
#else
#error unknown byte order
#endif
}

static void verifyTag(const uint8_t* source, const char* tag, size_t tagSize) {
    if (strncmp((const char*)source, tag, tagSize) != 0) {
        throw runtimeErrorWithMsg("Couldn't find tag \"", tag, "\" in source");
    }
}

static std::unique_ptr<QAudioClip> loadWav(const uint8_t fileBuffer[], size_t fileSize) {
    if (fileSize < 44) {
        throw std::runtime_error("Invalid WAVE file, too small");
    }
    verifyTag(fileBuffer, "RIFF", 4);
    size_t riffSize = parseUint32LE(fileBuffer + 4);
    if (riffSize > fileSize - 8) {
        throw std::runtime_error("Invalid WAVE file, RIFF chunk size is larger than remainder of fileSize");
    }
    verifyTag(fileBuffer + 8, "WAVE", 4);
    size_t readCursor = 12;
    const uint8_t* formatChunk = nullptr;
    const uint8_t* dataChunk = nullptr;
    uint32_t dataSize = 0;
    while (formatChunk == nullptr || dataChunk == nullptr) {
        if (readCursor > riffSize - 8) {
            throw std::runtime_error("Couldn't find fmt or data chunk in WAVE file");
        }
        uint32_t chunkSize = parseUint32LE(fileBuffer + readCursor + 4);
        if (readCursor + chunkSize > riffSize) {
            throw std::runtime_error("Invalid WAVE file, chunk leaves file boundary");
        }
        if (formatChunk == nullptr && strncmp((const char*)(fileBuffer + readCursor), "fmt ", 4) == 0) {
            formatChunk = fileBuffer + readCursor + 8;
            if (chunkSize < 16) {
                throw runtimeErrorWithMsg("Invalid WAVE file, fmt chunk is too small: ", chunkSize);
            }
        } else if (dataChunk == nullptr && strncmp((const char*)(fileBuffer + readCursor), "data", 4) == 0) {
            dataChunk = fileBuffer + readCursor + 8;
            dataSize = chunkSize;
        }
        readCursor += chunkSize + 8;
    }

    uint16_t channels = parseUint16LE(formatChunk + 2);
    uint32_t sampleRate = parseUint32LE(formatChunk + 4);
    uint16_t bitsPerSample = parseUint16LE(formatChunk + 14);
    AudioClipRawBuilder rawBuilder(dataChunk, dataSize);
    rawBuilder.bitDepth(bitsPerSample)
            .channels(channels)
            .sampleRate(sampleRate)
            .isSigned(bitsPerSample != 8);
    return rawBuilder.build();
}

std::unique_ptr<QAudioClip> AudioClipLoader::load(const char* pathToAudioFile) {
    std::ifstream ifStream{pathToAudioFile, std::ios::binary|std::ios::ate};
    if (!ifStream.is_open()) {
        throw runtimeErrorWithMsg("Couldn't open file: ", pathToAudioFile);
    } else {
        std::streamsize fileSize = ifStream.tellg();
        if (fileSize < std::streamoff{4}) {
            throw std::runtime_error("Invalid audio file, error on file operation or too small");
        }
        if (fileSize > std::numeric_limits<int>::max()) {
            throw std::runtime_error("Invalid audio file, too large");
        }
        ifStream.seekg(0, std::ios::beg);
        std::unique_ptr<uint8_t[]> fileBuffer{new uint8_t[fileSize]};
        ifStream.read((char*)fileBuffer.get(), fileSize);
        ifStream.close();
        const char* fileStart = (char*)fileBuffer.get();
        if (strncmp(fileStart, "OggS", 4) == 0) {
            return std::make_unique<AudioClipVorbis>(std::move(fileBuffer), fileSize);
        } else if (strncmp(fileStart, "RIFF", 4) == 0) {
            return loadWav(fileBuffer.get(), fileSize);
        } else if (strncmp(fileStart, "RIFX", 4) == 0) {
            throw runtimeErrorWithMsg("Big endian WAVE file not supported: ", pathToAudioFile);
        } else {
            throw runtimeErrorWithMsg("Invalid audio file: ", pathToAudioFile);
        }
    }
}

}
