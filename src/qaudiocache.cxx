#include "qaudiocache.hxx"
#include <vector>
#include "qaudioclip.hxx"
#include "audiocliploader.hxx"
#include "qassert.hxx"

namespace qz {

QAudioCache::QAudioCache() { }

std::shared_ptr<QAudioClip> QAudioCache::load(const std::string& filename) {
    auto emplaceRet = audioClips.emplace(filename, AudioClipLoader::load(filename.c_str()));
    return emplaceRet.first->second;
}

std::shared_ptr<QAudioClip> QAudioCache::load(const std::string& key, const std::string& filename) {
    auto emplaceRet = audioClips.emplace(key, AudioClipLoader::load(filename.c_str()));
    return emplaceRet.first->second;
}

std::shared_ptr<QAudioClip> QAudioCache::get(const std::string& key) {
    return audioClips.at(key);
}

void QAudioCache::remove(const std::string& key) {
    dyingClips.emplace(key, audioClips.at(key));
    audioClips.erase(key);
    prune();
}

void QAudioCache::removeUnused() {
    std::vector<std::string> keysToRemove;
    for (auto pair : audioClips) {
        if (pair.second.use_count() == 1) {
            dyingClips.emplace(pair.first, pair.second);
            keysToRemove.push_back(pair.first);
        }
    }
    for (const std::string& key : keysToRemove) {
        audioClips.erase(key);
    }
    prune();
}

void QAudioCache::prune() {
    std::vector<std::string> keysToRemove;
    for (auto pair : dyingClips) {
        if (pair.second.expired()) {
            keysToRemove.push_back(pair.first);
        }
    }
    for (const std::string& key : keysToRemove) {
        dyingClips.erase(key);
    }
}

}
