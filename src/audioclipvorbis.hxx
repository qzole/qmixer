#pragma once

#include "qaudioclip.hxx"
#include <memory>
#include <stdint.h>

struct stb_vorbis;

namespace qz {

class AudioClipVorbis : public QAudioClip {
public:
    AudioClipVorbis(std::unique_ptr<uint8_t[]> fileBuffer, size_t fileSize);
    size_t addSamples(float* output, size_t cursor, size_t sampleCount, int channelCount, int sampleRate, float volume) override;
private:
    std::unique_ptr<uint8_t[]> fileBuffer;
    stb_vorbis* vorbisHandle;
    size_t sampleRate;
    size_t channels;
    size_t sampleCount;
    size_t lastCursor;

    float internalBuffer[10000];
};

}
